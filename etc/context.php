<?php

$resolver->addRule(
    'Booking\\Framework\\Model\\Resource\\Connection',
    new \Booking\Framework\Di\Rule(null, require __DIR__ . DIRECTORY_SEPARATOR . 'connection.php', true)
);

$resolver->addRule(
    'Booking\\Customer\\Model\\Session',
    new \Booking\Framework\Di\Rule(null, [], true)
);

$resolver->addRule(
    'Booking\\Framework\\Model\\SessionInterface',
    new \Booking\Framework\Di\Rule('Booking\\Customer\\Model\\Session', [], true)
);

$router = $context->getRouter();
$router
    ->setDefault('Booking\\Commerce\\Controller\\Property\\Listing')
    ->setNotFound('Booking\\Framework\\Controller\\NotFound')
    ->addRoute('login', 'Booking\\Customer\\Controller\\Account\\Login')
    ->addRoute('logout', 'Booking\\Customer\\Controller\\Account\\Logout')
    ->addRoute('register', 'Booking\\Customer\\Controller\\Account\\Register')
;
