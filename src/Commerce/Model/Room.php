<?php

namespace Booking\Commerce\Model;

use \Booking\Framework\Model\AbstractModel;

class Room extends AbstractModel
{
    public function __construct(Resource\Room $resource)
    {
        parent::__construct($resource);

        $this->addFields(['room_id', 'property_id', 'name', 'quantity', 'price']);

        $this->setPrimaryField('room_id');
    }

    public function setPropertyId($propertyId)
    {
        $this->setData('property_id', $propertyId);
        return $this;
    }

    public function getPropertyId()
    {
        return $this->getData('property_id');
    }

    public function setName($name)
    {
        $this->setData('name', $name);
        return $this;
    }

    public function getName()
    {
        return $this->getData('name');
    }

    public function setQuantity($quantity)
    {
        $this->setData('quantity', $quantity);
        return $this;
    }

    public function getQuantity()
    {
        return $this->getData('quantity');
    }

    public function setPrice($price)
    {
        $this->setData('price', $price);
        return $this;
    }

    public function getPrice()
    {
        return $this->getData('price');
    }
}
