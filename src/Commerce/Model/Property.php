<?php

namespace Booking\Commerce\Model;

use \Booking\Framework\Model\AbstractModel;

class Property extends AbstractModel
{
    private $rooms;

    public function __construct(Resource\Property $resource, Resource\Room\Collection $rooms)
    {
        parent::__construct($resource);

        $this->addFields(['property_id', 'proprietor_id', 'name']);

        $this->setPrimaryField('property_id');

        $this->rooms = $rooms;
    }

    public function setProprietorId($proprietorId)
    {
        $this->setData('proprietor_id', $proprietorId);
        return $this;
    }

    public function getProprietorId()
    {
        return $this->getData('proprietor_id');
    }

    public function setName($name)
    {
        $this->setData('name', $name);
        return $this;
    }

    public function getName()
    {
        return $this->getData('name');
    }

    public function getRooms()
    {
        $this->rooms->load(['property_id' => $this->getId()]);
        return $this->rooms;
    }
}
