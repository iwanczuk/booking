<?php

namespace Booking\Commerce\Model\Resource;

use \Booking\Framework\Model\Resource\Connection;
use \Booking\Framework\Model\Resource\AbstractResource;

class Proprietor extends AbstractResource
{
    public function __construct(Connection $connection)
    {
        parent::__construct($connection);

        $this->setTable('commerce_proprietor');
    }
}
