<?php

namespace Booking\Commerce\Model\Resource\Room;

use \Booking\Framework\Model\Resource\Collection\AbstractCollection;
use \Booking\Framework\Model\Resource\Collection\FactoryInterface;
use \Booking\Framework\Model\Resource\Connection;

class Collection extends AbstractCollection
{
    public function __construct(Connection $connection, Factory $factory)
    {
        parent::__construct($connection, $factory);

        $this->setTable('commerce_room');
    }
}
