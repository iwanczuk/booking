<?php

namespace Booking\Commerce\Model\Resource\Room;

use \Booking\Framework\Http\ContextInterface;
use \Booking\Framework\Model\Resource\Collection\FactoryInterface;

class Factory implements FactoryInterface
{
    private $context;

    public function __construct(ContextInterface $context)
    {
        $this->context = $context;
    }

    public function create()
    {
        return $this->context->getResolver()->create('Booking\\Commerce\\Model\\Room');
    }
}
