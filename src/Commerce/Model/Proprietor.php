<?php

namespace Booking\Commerce\Model;

use \Booking\Framework\Model\AbstractModel;

class Proprietor extends AbstractModel
{
    public function __construct(Resource\Proprietor $resource)
    {
        parent::__construct($resource);

        $this->addFields(['proprietor_id', 'name']);

        $this->setPrimaryField('proprietor_id');
    }
}
