<?php

namespace Booking\Customer\Model;

use \Booking\Framework\Model\AbstractModel;

class Order extends AbstractModel
{
    const STATE_PENDING   = 1;
    const STATE_CANCELED  = 2;
    const STATE_COMPLETED = 3;

    public function __construct(Resource\Order $resource)
    {
        parent::__construct($resource);

        $this->addFields(['order_id', 'room_id', 'account_id', 'from_date', 'to_date', 'price', 'state']);

        $this->setPrimaryField('order_id');
    }
}
