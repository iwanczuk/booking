<?php

namespace Booking\Commerce\Controller\Property;

use \Booking\Framework\Http\ContextInterface;
use \Booking\Commerce\View\Property\ListingPage;
use \Booking\Commerce\Model\Resource\Property\Collection;

class Listing extends \Booking\Framework\Controller\AbstractAction
{
    private $page;
    private $collection;

    public function __construct(ContextInterface $context, ListingPage $page, Collection $collection)
    {
        parent::__construct($context);

        $this->page = $page;
        $this->collection = $collection;
    }

    public function execute()
    {
        $this->collection->load();
        $this->page->getChild()->setCollection($this->collection);
        $this->getResponse()->setBody($this->page->render());
    }
}
