<?php

namespace Booking\Commerce\View\Property;

use \Booking\Framework\Http\ContextInterface;
use \Booking\Framework\View\Page;
use \Booking\Framework\Model\SessionInterface;

class ListingPage extends Page
{
    public function __construct(ContextInterface $context, Listing $listing, SessionInterface $session)
    {
        parent::__construct($context, $listing, $session);
    }
}
