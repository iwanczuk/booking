<?php

namespace Booking\Commerce\View\Property;

use \Booking\Framework\Http\ContextInterface;
use \Booking\Framework\View\Template;

class Listing extends Template
{
    private $collection;

    public function __construct(ContextInterface $context)
    {
        parent::__construct($context);

        $this->setTemplate(
            __DIR__ . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR .
            '..' . DIRECTORY_SEPARATOR . 'template'. DIRECTORY_SEPARATOR .
            'property' . DIRECTORY_SEPARATOR . 'listing.phtml'
        );
    }

    public function setCollection($collection)
    {
        $this->collection = $collection;
        return $this;
    }

    public function getCollection()
    {
        return $this->collection;
    }
}
