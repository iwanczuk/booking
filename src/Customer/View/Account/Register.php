<?php

namespace Booking\Customer\View\Account;

use \Booking\Framework\Http\ContextInterface;
use \Booking\Framework\View\Template;

class Register extends Template
{
    public function __construct(ContextInterface $context)
    {
        parent::__construct($context);

        $this->setTemplate(
            __DIR__ . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR .
            '..' . DIRECTORY_SEPARATOR . 'template' . DIRECTORY_SEPARATOR .
            'account' . DIRECTORY_SEPARATOR . 'register.phtml'
        );
    }
}
