<?php

namespace Booking\Customer\View\Account;

use \Booking\Framework\Http\ContextInterface;
use \Booking\Framework\View\Page;
use \Booking\Framework\Model\SessionInterface;

class LoginPage extends Page
{
    public function __construct(ContextInterface $context, Login $login, SessionInterface $session)
    {
        parent::__construct($context, $login, $session);
    }
}
