<?php

namespace Booking\Customer\View\Account;

use \Booking\Framework\Http\ContextInterface;
use \Booking\Framework\View\Page;
use \Booking\Framework\Model\SessionInterface;

class RegisterPage extends Page
{
    public function __construct(ContextInterface $context, Register $register, SessionInterface $session)
    {
        parent::__construct($context, $register, $session);
    }
}
