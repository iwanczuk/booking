<?php

namespace Booking\Customer\Controller\Account;

use \Booking\Framework\Http\ContextInterface;
use \Booking\Customer\Model\Session;

class Logout extends \Booking\Framework\Controller\AbstractAction
{
    private $session;

    public function __construct(ContextInterface $context, Session $session)
    {
        parent::__construct($context);

        $this->session = $session;
    }

    public function execute()
    {
        if ($this->session->isLoggedIn()) {
            $this->session->logout();
        }
        $this->redirect();
    }
}
