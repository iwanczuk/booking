<?php

namespace Booking\Customer\Controller\Account;

use \Booking\Framework\Http\ContextInterface;
use \Booking\Customer\Model\Account;
use \Booking\Customer\Model\Session;
use \Booking\Customer\View\Account\RegisterPage;

class Register extends \Booking\Framework\Controller\AbstractAction
{
    private $page;
    private $session;
    private $account;

    public function __construct(ContextInterface $context, Session $session, RegisterPage $page, Account $account)
    {
        parent::__construct($context);

        $this->session = $session;
        $this->page    = $page;
        $this->account = $account;
    }

    public function execute()
    {
        if ($this->session->isLoggedIn()) {
            $this->redirect();
            return;
        }
        if ($this->getRequest()->isPost()) {
            $register = $this->getRequest()->getPost()->get('register');
            $email    = isset($register['email']) ? trim($register['email']) : '';
            $password = isset($register['password']) ? trim($register['password']) : '';
            try {
                $this->account->setEmail($email);
                $this->account->setPassword($password);
                $this->account->save();
                $this->session->login($email, $password);
                $this->redirect();
                return;
            } catch (\Exception $e) {
                $this->page->setMessage($e->getMessage());
            }
        }
        $this->getResponse()->setBody($this->page->render());
    }
}
