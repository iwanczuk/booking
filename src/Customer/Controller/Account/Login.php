<?php

namespace Booking\Customer\Controller\Account;

use \Booking\Framework\Http\ContextInterface;
use \Booking\Customer\Model\Session;
use \Booking\Customer\View\Account\LoginPage;

class Login extends \Booking\Framework\Controller\AbstractAction
{
    private $page;
    private $session;

    public function __construct(ContextInterface $context, Session $session, LoginPage $page)
    {
        parent::__construct($context);

        $this->session = $session;
        $this->page    = $page;
    }

    public function execute()
    {
        if ($this->session->isLoggedIn()) {
            $this->redirect();
            return;
        }
        if ($this->getRequest()->isPost()) {
            $login    = $this->getRequest()->getPost()->get('login');
            $email    = isset($login['email']) ? trim($login['email']) : '';
            $password = isset($login['password']) ? trim($login['password']) : '';
            try {
                $this->session->login($email, $password);
                $this->redirect();
                return;
            } catch (\Exception $e) {
                $this->page->setMessage($e->getMessage());
            }
        }
        $this->getResponse()->setBody($this->page->render());
    }
}
