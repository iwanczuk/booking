<?php

namespace Booking\Customer\Model;

use \Booking\Framework\Model\AbstractModel;

class Account extends AbstractModel
{
    public function __construct(Resource\Account $resource)
    {
        parent::__construct($resource);

        $this->addFields(['account_id', 'email', 'password']);

        $this->setPrimaryField('account_id');
    }

    public function setEmail($email)
    {
        if (!preg_match('/^.+@[^@]+$/', $email)) {
            throw new \Exception('Invalid email address.');
        }
        $this->setData('email', $email);
        return $this;
    }

    public function getEmail()
    {
        return $this->getData('email');
    }

    public function setPassword($password)
    {
        if (strlen($password) < 6) {
            throw new \Exception('Password is too short.');
        }
        $this->setData('password', password_hash($password, \PASSWORD_BCRYPT));
        return $this;
    }

    public function getPassword()
    {
        return $this->getData('password');
    }

    public function loadByEmail($email)
    {
        $this->getResource()->loadByEmail($this, $email);
        return $this;
    }

    public function isEmailUnique($email)
    {
        return $this->getResource()->isEmailUnique($this, $email);
    }

    public function authenticate($email, $password)
    {
        $this->loadByEmail($email);
        if (!$this->getId() || !$this->validatePassword($password)) {
            throw new \Exception('Invalid email or password.');
        }
        return $this;
    }

    public function validatePassword($password)
    {
        return password_verify($password, $this->getData('password'));
    }

    public function save()
    {
        if (!$this->isEmailUnique($this->getEmail())) {
            throw new \Exception('Email address is already in use.');
        }
        return parent::save();
    }
}
