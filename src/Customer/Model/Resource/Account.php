<?php

namespace Booking\Customer\Model\Resource;

use \Booking\Framework\Model\AbstractModel;
use \Booking\Framework\Model\Resource\Connection;
use \Booking\Framework\Model\Resource\AbstractResource;

class Account extends AbstractResource
{
    public function __construct(Connection $connection)
    {
        parent::__construct($connection);

        $this->setTable('customer_account');
    }

    public function loadByEmail(AbstractModel $object, $email)
    {
        return $this->load($object, $email, 'email');
    }

    public function isEmailUnique(AbstractModel $object, $email)
    {
        $condition = [
            'account_id' => ['neq' => $object->getId()],
            'email'      => $email
        ];
        $select = $this->getConnection()->select($this->getTable(), ['account_id'], $condition, 1);
        $data = $this->getConnection()->fetchRow($select);
        return !$data;
    }
}
