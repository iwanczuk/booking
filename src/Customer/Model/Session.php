<?php

namespace Booking\Customer\Model;

use \Booking\Framework\Model\SessionInterface;

class Session implements SessionInterface
{
    private $account;

    public function __construct(Account $account)
    {
        $this->account = $account;

        session_start();

        if ($this->isLoggedIn()) {
            $this->account->load($_SESSION['account_id']);
        }
    }

    public function __destruct()
    {
        session_write_close();
    }

    public function getAccount()
    {
        return $this->account;
    }

    public function isLoggedIn()
    {
        return isset($_SESSION['account_id']);
    }

    public function login($email, $password)
    {
        if (!$this->isLoggedIn()) {
            $this->getAccount()->authenticate($email, $password);
            $_SESSION['account_id'] = $this->getAccount()->getId();
        }
        return $this;
    }

    public function logout()
    {
        unset($_SESSION['account_id']);
    }
}
