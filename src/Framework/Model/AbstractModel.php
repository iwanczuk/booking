<?php

namespace Booking\Framework\Model;

use \Booking\Framework\Model\Resource\AbstractResource;

class AbstractModel
{
    private $data;

    private $resource;

    private $fields;

    private $primary;

    public function __construct(AbstractResource $resource)
    {
        $this->resource = $resource;
        $this->data     = [];
        $this->fields   = [];
    }

    public function setData($key, $value = null)
    {
        if (is_array($key)) {
            foreach ($key as $index => $val) {
                $this->setData($index, $val);
            }
        } elseif (is_string($key)) {
            if (!array_key_exists($key, $this->fields)) {
                throw new \LogicException();
            }
            $this->data[$key] = $value;
        } else {
            throw new \InvalidArgumentException();
        }
        return $this;
    }

    public function getData($key = null)
    {
        if (null === $key) {
            return $this->data;
        }
        if (isset($this->data[$key])) {
            return $this->data[$key];
        }
    }

    public function hasData($key)
    {
        return array_key_exists($key, $this->fields);
    }

    public function getId()
    {
        return $this->getData($this->getPrimaryField());
    }

    public function setId($value)
    {
        $this->setData($this->getPrimaryField(), $value);
        return $this;
    }

    public function getResource()
    {
        return $this->resource;
    }

    public function getFields()
    {
        return array_keys($this->fields);
    }

    public function getPrimaryField()
    {
        return $this->primary;
    }

    public function load($id)
    {
        $this->getResource()->load($this, $id);
        return $this;
    }

    public function save()
    {
        $this->getResource()->save($this);
        return $this;
    }

    protected function addField($name)
    {
        if (array_key_exists($name, $this->fields)) {
            throw new \LogicException();
        }
        $this->fields[$name] = true;
        return $this;
    }

    protected function addFields(array $fields)
    {
        foreach ($fields as $name) {
            $this->addField($name);
        }
        return $this;
    }

    protected function setPrimaryField($name)
    {
        if (!array_key_exists($name, $this->fields)) {
            throw new \LogicException();
        }
        $this->primary = $name;
    }
}
