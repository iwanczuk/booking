<?php

namespace Booking\Framework\Model;

interface SessionInterface
{
    public function isLoggedIn();
}
