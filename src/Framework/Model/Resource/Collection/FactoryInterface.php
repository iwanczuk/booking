<?php

namespace Booking\Framework\Model\Resource\Collection;

interface FactoryInterface
{
    public function create();
}