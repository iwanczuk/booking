<?php

namespace Booking\Framework\Model\Resource\Collection;

use \Booking\Framework\Model\AbstractModel;
use \Booking\Framework\Model\Resource\Connection;

class AbstractCollection implements \IteratorAggregate, \Countable
{
    private $connection;

    private $factory;

    private $items;

    private $loaded;

    private $table;

    public function __construct(Connection $connection, FactoryInterface $factory)
    {
        $this->connection = $connection;
        $this->factory    = $factory;
        $this->items      = [];
        $this->loaded     = false;
    }

    public function getConnection()
    {
        return $this->connection;
    }

    public function isLoaded()
    {
        return $this->loaded;
    }

    public function setTable($table)
    {
        $this->table = $table;
        return $this;
    }

    public function getTable()
    {
        return $this->table;
    }

    public function load(array $where = [])
    {
        if ($this->isLoaded()) {
            return $this;
        }
        $select = $this->getConnection()->select($this->getTable(), ['*'], $where);
        $data = $this->getConnection()->fetchAll($select);
        if (is_array($data)) {
            $this->loaded = true;
            foreach ($data as $row) {
                $object = $this->factory->create();
                $object->setData($row);
                $this->addItem($object);
            }
        }
        return $this;
    }

    public function getItems()
    {
        return $this->items;
    }

    public function addItem(AbstractModel $object)
    {
        if (!is_null($object->getId())) {
            if (isset($this->items[$object->getId()])) {
                throw new \Exception('Object with the same ID already exists.');
            }
            $this->items[$object->getId()] = $object;
        } else {
            $this->items[] = $object;
        }
        return $this;
    }

    public function getIterator()
    {
        return new \ArrayIterator($this->items);
    }

    public function count()
    {
        return count($this->items);
    }
}
