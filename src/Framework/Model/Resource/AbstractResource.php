<?php

namespace Booking\Framework\Model\Resource;

use \Booking\Framework\Model\AbstractModel;

abstract class AbstractResource
{
    private $connection;

    private $table;

    public function __construct(Connection $connection)
    {
        $this->connection = $connection;
    }

    public function getConnection()
    {
        return $this->connection;
    }

    public function setTable($table)
    {
        $this->table = $table;
        return $this;
    }

    public function getTable()
    {
        return $this->table;
    }

    public function load(AbstractModel $object, $value, $field = null)
    {
        if (is_null($field)) {
            $field = $object->getPrimaryField();
        }
        $condition = [$field => $this->getConnection()->quote($value)];
        $select = $this->getConnection()->select($this->getTable(), $object->getFields(), $condition, 1);
        $data = $this->getConnection()->fetchRow($select);
        if ($data) {
            $object->setData($data);
        }
        return $this;
    }

    public function save(AbstractModel $object)
    {
        if (!is_null($object->getId())) {
            $condition[$object->getPrimaryField()] = $object->getId();
            $data = $this->_prepareDataForSave($object);
            unset($data[$object->getPrimaryField()]);
            $this->getConnection()->update($this->getTable(), $data, $condition);
        } else {
            $bind = $this->_prepareDataForSave($object);
            unset($bind[$object->getPrimaryField()]);
            $this->getConnection()->insert($this->getTable(), $bind);
            $object->setId($this->getConnection()->lastInsertId());
        }
        return $this;
    }

    protected function _prepareDataForSave(AbstractModel $object)
    {
        $data = array();
        foreach ($object->getFields() as $name) {
            if ($object->hasData($name)) {
                $data[$name] = $this->getConnection()->quote($object->getData($name));
            }
        }
        return $data;
    }
}
