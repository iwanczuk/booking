<?php

namespace Booking\Framework\Model\Resource;

use \mysqli;

class Connection
{
    private $link;

    public function __construct($host, $username, $password, $dbname, $port = 3306)
    {
        $this->link = new mysqli($host, $username, $password, $dbname, $port);
    }

    public function __destruct()
    {
        if ($this->link) {
            $this->link->close();
        }
    }

    public function select($table, array $columns, array $where = [], $limit = null, $offset = null, $order = null)
    {
        $sql = 'SELECT ' . implode(', ', $columns) . ' FROM ' . $table;
        if ($where) {
            $condition = [];
            foreach ($where as $field => $value) {
                if (is_array($value)) {
                    if (array_key_exists('neq', $value)) {
                        $operator = '<>';
                        $param = $value['neq'];
                    } elseif (array_key_exists('eq', $value)) {
                        $operator = '=';
                        $param = $value['eq'];
                    }
                } else {
                    $operator = '=';
                    $param    = $value;
                }
                $condition[] = $field . ' ' . $operator . ' \'' . $param . '\'';
            }
            $sql .= ' WHERE ' . implode(' AND ', $condition);
        }
        if ($limit) {
            $sql .= ' LIMIT ' . $limit;
        }
        if ($offset) {
            $sql .= ' OFFSET ' . $offset;
        }
        if ($order) {
            $sql .= ' ORDER BY ' . $order;
        }
        return $sql;
    }

    public function fetchRow($sql)
    {
        $result = $this->link->query($sql);
        if ($result) {
            return $result->fetch_assoc();
        } else {
            throw new \RuntimeException();
        }
    }

    public function fetchAll($sql)
    {
        $result = $this->link->query($sql);
        if ($result) {
            return $result->fetch_all(\MYSQLI_ASSOC);
        } else {
            throw new \RuntimeException();
        }
    }

    public function insert($table, array $data)
    {
        $columns = [];
        $sql = 'INSERT INTO ' . $table . ' SET ';
        foreach ($data as $field => $value) {
            $columns[] = $field . ' = \'' . $value . '\'';
        }
        $sql .= implode(', ', $columns);
        return $this->link->query($sql);
    }

    public function update($table, array $data, array $where = [])
    {
        $columns = [];
        $sql = 'UPDATE ' . $table . ' SET ';
        foreach ($data as $field => $value) {
            $columns[] = $field . ' = \'' . $value . '\'';
        }
        $sql .= implode(', ', $columns);
        if ($where) {
            $condition = [];
            foreach ($where as $field => $value) {
                $condition[] = $field . ' = \'' . $value . '\'';
            }
            $sql .= ' WHERE ' . implode(' AND ', $condition);
        }
        return $this->link->query($sql);
    }

    public function lastInsertId()
    {
        return $this->link->insert_id;
    }

    public function quote($string)
    {
        return $this->link->real_escape_string($string);
    }
}
