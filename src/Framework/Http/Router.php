<?php

namespace Booking\Framework\Http;

class Router implements RouterInterface
{
    private $default;
    private $notfound;
    private $routes;

    public function __construct()
    {
        $this->default  = '';
        $this->notfound = '';
        $this->routes   = [];
    }

    public function setDefault($controller)
    {
        $this->default = $controller;
        return $this;
    }

    public function setNotFound($controller)
    {
        $this->notfound = $controller;
        return $this;
    }

    public function addRoute($path, $controller)
    {
        $this->routes[$path] = $controller;
        return $this;
    }

    public function dispatch($path)
    {
        if ($path == '') {
            return $this->default;
        }
        if (array_key_exists($path, $this->routes)) {
            return $this->routes[$path];
        }
        return $this->notfound;
    }
}
