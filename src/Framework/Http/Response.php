<?php

namespace Booking\Framework\Http;

class Response implements ResponseInterface
{
    private $code;
    private $body;
    private $headers;

    public function __construct()
    {
        $this->code    = 200;
        $this->body    = '';
        $this->headers = [];
    }

    public function setLocation($url)
    {
        $this->setHeader('Location', $url);
        return $this;
    }

    public function getCode()
    {
        return $this->code;
    }

    public function setCode($code)
    {
        $this->code = $code;
        return $this;
    }

    public function setHeader($name, $value)
    {
        $this->headers[] = $name . ': ' . $value;
        return $this;
    }

    public function setBody($body)
    {
        $this->body = $body;
        return $this;
    }

    public function sendHeaders()
    {
        if (headers_sent()) {
            throw new \LogicException('Headers already sent.');
        }

        http_response_code($this->getCode());

        foreach ($this->headers as $header) {
            header($header);
        }
    }

    public function sendBody()
    {
        echo $this->body;
    }

    public function send()
    {
        $this->sendHeaders();
        $this->sendBody();
    }
}
