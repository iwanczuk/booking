<?php

namespace Booking\Framework\Http;

interface RouterInterface
{
    public function setDefault($controller);

    public function setNotFound($controller);

    public function addRoute($path, $controller);

    public function dispatch($path);
}
