<?php

namespace Booking\Framework\Http;

class Container
{
    private $data;

    public function __construct(array $data = [])
    {
        $this->data = $data;
    }

    public function get($key = null, $default = null)
    {
        if (is_null($key)) {
            return $this->data;
        }
        if (!is_string($key)) {
            throw new \InvalidArgumentException('Invalid key type.');
        }
        if (array_key_exists($key, $this->data)) {
            return $this->data[$key];
        } else {
            return $default;
        }
    }
}
