<?php

namespace Booking\Framework\Http;

interface ResponseInterface
{
    public function setLocation($url);

    public function setHeader($name, $value);

    public function setBody($body);

    public function sendHeaders();

    public function sendBody();

    public function send();
}