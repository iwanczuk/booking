<?php

namespace Booking\Framework\Http;

interface RequestInterface
{
    /**
     * @return Booking\Framework\Http\Container
     */
    public function getQuery();

    /**
     * @return Booking\Framework\Http\Container
     */
    public function getPost();

    /**
     * @return Booking\Framework\Http\Container
     */
    public function getServer();

    /**
     * @return string
     */
    public function getMethod();

    /**
     * @return bool
     */
    public function isGet();

    /**
     * @return bool
     */
    public function isPost();

    /**
     * @return string
     */
    public function getBaseUrl();

    /**
     * @return string
     */
    public function getPathInfo();
}