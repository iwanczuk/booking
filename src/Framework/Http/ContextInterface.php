<?php

namespace Booking\Framework\Http;

interface ContextInterface
{
    /**
     * @return Booking\Framework\Di\Resolver
     */
    public function getResolver();

    /**
     * @return Booking\Framework\Http\RequestInterface
     */
    public function getRequest();

    /**
     * @return Booking\Framework\Http\ResponseInterface
     */
    public function getResponse();
}