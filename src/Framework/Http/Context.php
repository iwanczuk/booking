<?php

namespace Booking\Framework\Http;

use Booking\Framework\Di\Resolver;
use Booking\Framework\Di\Rule;

class Context implements ContextInterface
{
    /** @var Booking\Framework\Di\ResolverInterface */
    private $resolver;

    /** @var Booking\Framework\Http\RequestInterface */
    private $request;

    /** @var Booking\Framework\Http\ResponseInterface */
    private $response;

    /** @var Booking\Framework\Http\RouterInterface */
    private $router;

    public function __construct(Resolver $resolver, RequestInterface $request, ResponseInterface $response, RouterInterface $router)
    {
        $this->resolver = $resolver;
        $this->request  = $request;
        $this->response = $response;
        $this->router   = $router;
    }

    /**
     * @return Booking\Framework\Di\Resolver
     */
    public function getResolver()
    {
        return $this->resolver;
    }

    /**
     * @return Booking\Framework\Http\RequestInterface
     */
    public function getRequest()
    {
        return $this->request;
    }

    /**
     * @return Booking\Framework\Http\ResponseInterface
     */
    public function getResponse()
    {
        return $this->response;
    }

    /**
     * @return Booking\Framework\Http\RouterInterface
     */
    public function getRouter()
    {
        return $this->router;
    }

    /**
     * @return Booking\Framework\Http\ContextInterface
     */
    public static function init()
    {
        $resolver = new Resolver();
        $resolver->addRule(
            'Booking\\Framework\\Http\\RequestInterface',
            new Rule('Booking\\Framework\\Http\\Request', [$_GET, $_POST, $_SERVER])
        );
        $resolver->addRule(
            'Booking\\Framework\\Http\\ResponseInterface',
            new Rule('Booking\\Framework\\Http\\Response')
        );
        $resolver->addRule(
            'Booking\\Framework\\Http\\RouterInterface',
            new Rule('Booking\\Framework\\Http\\Router')
        );
        $resolver->addRule(
            'Booking\\Framework\\Http\\ContextInterface',
            new Rule('Booking\\Framework\\Http\\Context', [], true)
        );

        $context = $resolver->create('Booking\\Framework\\Http\\ContextInterface');

        require __DIR__ . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'etc' . DIRECTORY_SEPARATOR . 'context.php';

        return $context;
    }

    public function handle()
    {
        try {
            $this->handleRequest();
        } catch (\Exception $e) {
            $this->handleException($e);
        }
    }

    public function handleRequest()
    {
        $controller = $this->getResolver()->create(
            $this->getRouter()->dispatch(
                $this->getRequest()->getPathInfo()
            )
        );
        $controller->execute();
        $this->getResponse()->send();
    }

    public function handleException(\Exception $exception)
    {
        header('Status: 503 Service Unavailable', true, 503);
        echo 'There has been an error processing your request.';
    }
}
