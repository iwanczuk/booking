<?php

namespace Booking\Framework\Http;

class Request implements RequestInterface
{
    /** @var Booking\Framework\Http\Container */
    private $query;

    /** @var Booking\Framework\Http\Container */
    private $post;

    /** @var Booking\Framework\Http\Container */
    private $server;

    private $baseUrl;

    private $pathInfo;

    public function __construct(array $query = [], array $post = [], array $server = [])
    {
        $this->query  = new Container($query);
        $this->post   = new Container($post);
        $this->server = new Container($server);
    }

    /**
     * @return Booking\Framework\Http\Container
     */
    public function getQuery()
    {
        return $this->query;
    }

    /**
     * @return Booking\Framework\Http\Container
     */
    public function getPost()
    {
        return $this->post;
    }

    /**
     * @return Booking\Framework\Http\Container
     */
    public function getServer()
    {
        return $this->server;
    }

    /**
     * @return string
     */
    public function getMethod()
    {
        return $this->getServer()->get('REQUEST_METHOD');
    }

    /**
     * @return bool
     */
    public function isGet()
    {
        return $this->getMethod() == 'GET';
    }

    /**
     * @return bool
     */
    public function isPost()
    {
        return $this->getMethod() == 'POST';
    }

    /**
     * @return string
     */
    public function getBaseUrl()
    {
        if (is_null($this->baseUrl)) {
            $baseUrl    = $this->getServer()->get('SCRIPT_NAME');
            $requestUri = $this->getServer()->get('REQUEST_URI');
            if (0 === strpos($requestUri, dirname($baseUrl))) {
                $baseUrl = dirname($baseUrl) . '/';
            }
            $this->baseUrl = $baseUrl;
        }
        return $this->baseUrl;
    }

    /**
     * @return string
     */
    public function getPathInfo()
    {
        if (is_null($this->pathInfo)) {
            $requestUri = $this->getServer()->get('REQUEST_URI');
            $pos = strpos($requestUri, '?');
            if ($pos) {
                $requestUri = substr($requestUri, 0, $pos);
            }
            $pathInfo = trim(substr($requestUri, strlen($this->getBaseUrl())), '/');
            $this->pathInfo = $pathInfo;
        }
        return $this->pathInfo;
    }
}
