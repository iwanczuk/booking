<?php

namespace Booking\Framework\Controller;

use \Booking\Framework\Http\ContextInterface;

abstract class AbstractAction
{
    private $context;

    public function __construct(ContextInterface $context)
    {
        $this->context = $context;
    }

    public function getContext()
    {
        return $this->context;
    }

    public function getRequest()
    {
        return $this->getContext()->getRequest();
    }

    public function getResponse()
    {
        return $this->getContext()->getResponse();
    }

    abstract public function execute();

    protected function redirect($path = '')
    {
        $this->getResponse()->setLocation($this->getRequest()->getBaseUrl() . $path);
    }
}
