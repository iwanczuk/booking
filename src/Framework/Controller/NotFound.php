<?php

namespace Booking\Framework\Controller;

use \Booking\Framework\Http\ContextInterface;
use \Booking\Framework\View\NotFound as NotFoundView;

class NotFound extends AbstractAction
{
    private $page;

    public function __construct(ContextInterface $context, NotFoundView $page)
    {
        parent::__construct($context);

        $this->page = $page;
    }

    public function execute()
    {
        $this->getResponse()->setCode(404);
        $this->getResponse()->setBody($this->page->render());
    }
}
