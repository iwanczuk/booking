<?php

namespace Booking\Framework\Di;

class Rule
{
    private $instance;
    private $params;
    private $shared;

    public function __construct($instance = null, array $params = [], $shared = false)
    {
        $this
            ->setInstance($instance)
            ->setParams($params)
            ->setShared($shared)
        ;
    }

    public function getInstance()
    {
        return $this->instance;
    }

    public function setInstance($instance)
    {
        $this->instance = $instance;
        return $this;
    }

    public function getParams()
    {
        return $this->params;
    }

    public function setParams(array $params = [])
    {
        $this->params = $params;
        return $this;
    }

    public function getShared()
    {
        return $this->shared;
    }

    public function setShared($shared)
    {
        $this->shared = (bool) $shared;
        return $this;
    }
}
