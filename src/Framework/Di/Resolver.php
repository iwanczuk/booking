<?php

namespace Booking\Framework\Di;

class Resolver
{
    private $rules;
    private $cache;
    private $shared;

    public function __construct()
    {
        $this->rules  = [];
        $this->cache  = [];
        $this->shared = [__CLASS__ => $this];
    }

    public function addRule($type, Rule $rule)
    {
        if (!is_string($type)) {
            throw new \InvalidArgumentException('Type must be a string.');
        }
        $this->rules[ltrim(strtolower($type), '\\')] = $rule;
        return $this;
    }

    public function getRule($type)
    {
        if (!is_string($type)) {
            throw new \InvalidArgumentException('Type must be a string.');
        }
        if (isset($this->rules[strtolower(ltrim($type, '\\'))])) {
            return $this->rules[strtolower(ltrim($type, '\\'))];
        }
    }

    public function create($type, array $args = [])
    {
        $rule = $this->getRule($type);
        $preferedType = $rule ? ($rule->getInstance() ?: $type) : $type;
        if (isset($this->shared[$preferedType])) {
            return $this->shared[$preferedType];
        }
        if (empty($this->cache[$preferedType])) {
            $rule = $rule ?: new Rule;
            $class = new \ReflectionClass($preferedType);
            $constructor = $class->getConstructor();
            $params = $constructor ? $this->getParams($constructor, $rule) : null;
            $this->cache[$preferedType] = function($args) use ($preferedType, $rule, $class, $constructor, $params) {
                if ($rule->getShared()) {
                    $this->shared[$preferedType] = $object = $class->newInstanceWithoutConstructor();
                    if ($constructor) {
                        $constructor->invokeArgs($object, $params($args));
                    }
                } else {
                    $object = $params ? new $class->name(...$params($args)) : new $class->name;
                }
                return $object;
            };
        }
        return $this->cache[$preferedType]($args);
    }

    private function getParams(\ReflectionMethod $method, Rule $rule)
    {
        $paramInfo = [];
        foreach ($method->getParameters() as $param) {
            $type = $param->getClass() ? $param->getClass()->name : null;
            $paramInfo[] = [$type, $param->allowsNull()];
        }
        return function($args) use ($paramInfo, $rule) {
            if ($rule->getParams()) {
                $args = array_merge($args, $rule->getParams());
            }
            $parameters = [];
            foreach ($paramInfo as list($type, $allowsNull)) {
                if ($args) {
                    for ($i = 0; $i < count($args); $i++) {
                        if ($type && $args[$i] instanceof $type || !$args[$i] && $allowsNull) {
                            $parameters[] = array_splice($args, $i, 1)[0];
                            continue 2;
                        }
                    }
                }
                if ($type) {
                    $parameters[] = $this->create($type);
                } elseif ($args) {
                    $parameters[] = array_shift($args);
                }
            }
            return $parameters;
        };
    }
}
