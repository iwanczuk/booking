<?php

namespace Booking\Framework\View;

use \Booking\Framework\Http\ContextInterface;

abstract class AbstractView
{
    private $context;

    public function __construct(ContextInterface $context)
    {
        $this->context = $context;
    }

    public function getContext()
    {
        return $this->context;
    }

    public function escapeHtml($input)
    {
        return htmlspecialchars($input, ENT_COMPAT, 'UTF-8', false);
    }

    public function getUrl($path = '')
    {
        return $this->getContext()->getRequest()->getBaseUrl() . $path;
    }

    final public function render()
    {
        return $this->_render();
    }

    abstract protected function _render();
}
