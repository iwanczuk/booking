<?php

namespace Booking\Framework\View;

class Template extends AbstractView
{
    private $template;

    public function getTemplate()
    {
        return $this->template;
    }

    public function setTemplate($template)
    {
        $this->template = $template;
        return $this;
    }

    protected function _render()
    {
        ob_start();
        try {
            include $this->getTemplate();
        } catch (\Exception $e) {
            ob_get_clean();
            throw $e;
        }
        $html = ob_get_clean();
        return $html;
    }
}
