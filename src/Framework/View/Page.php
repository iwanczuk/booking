<?php

namespace Booking\Framework\View;

use \Booking\Framework\Http\ContextInterface;
use \Booking\Framework\Model\SessionInterface;

class Page extends Template
{
    private $child;
    private $message;
    private $session;

    public function __construct(ContextInterface $context, AbstractView $child, SessionInterface $session)
    {
        parent::__construct($context);

        $this->child   = $child;
        $this->session = $session;

        $this->setTemplate(__DIR__ . DIRECTORY_SEPARATOR .'..' . DIRECTORY_SEPARATOR .'template' . DIRECTORY_SEPARATOR .'page.phtml');
    }

    public function setMessage($message)
    {
        $this->message = $message;
        return $this;
    }

    public function getMessage()
    {
        return $this->message;
    }

    public function getChild()
    {
        return $this->child;
    }

    public function isLoggedIn()
    {
        return $this->session->isLoggedIn();
    }
}
