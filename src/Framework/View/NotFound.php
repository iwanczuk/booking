<?php

namespace Booking\Framework\View;

use \Booking\Framework\Http\ContextInterface;

class NotFound extends Template
{
    public function __construct(ContextInterface $context)
    {
        parent::__construct($context);

        $this->setTemplate(__DIR__ . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'template' . DIRECTORY_SEPARATOR . 'notfound.phtml');
    }
}
