CREATE TABLE customer_account
(
    account_id INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
    email VARCHAR(64) NOT NULL,
    password VARCHAR(255) NOT NULL,
    PRIMARY KEY (account_id),
    UNIQUE KEY unq_customer_account_email (email)
)
ENGINE=INNODB DEFAULT CHARSET=utf8;

CREATE TABLE commerce_proprietor
(
    proprietor_id INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
    name VARCHAR(255) NOT NULL,
    PRIMARY KEY (proprietor_id)
)
ENGINE=INNODB DEFAULT CHARSET=utf8;

CREATE TABLE commerce_property
(
    property_id INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
    proprietor_id INT(10) UNSIGNED NOT NULL,
    name VARCHAR(255) NOT NULL,
    PRIMARY KEY (property_id),
    KEY idx_commerce_property_proprietor_id (proprietor_id),
    CONSTRAINT fk_commerce_property_proprietor_id
        FOREIGN KEY (proprietor_id)
        REFERENCES commerce_proprietor (proprietor_id) ON DELETE CASCADE ON UPDATE CASCADE
)
ENGINE=INNODB DEFAULT CHARSET=utf8;

CREATE TABLE commerce_room
(
    room_id INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
    property_id INT(10) UNSIGNED NOT NULL,
    name VARCHAR(255) NOT NULL,
    quantity INT(10) UNSIGNED NOT NULL,
    price DECIMAL(12,4) UNSIGNED NOT NULL,
    PRIMARY KEY (room_id),
    KEY idx_commerce_room_property_id (property_id),
    CONSTRAINT fk_commerce_room_property_id
        FOREIGN KEY (property_id)
        REFERENCES commerce_property (property_id) ON DELETE CASCADE ON UPDATE CASCADE
)
ENGINE=INNODB DEFAULT CHARSET=utf8;

CREATE TABLE commerce_order
(
    order_id INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
    room_id INT(10) UNSIGNED NULL,
    account_id INT(10) UNSIGNED NULL,
    from_date DATE NOT NULL,
    to_date DATE NOT NULL,
    price DECIMAL(12,4) UNSIGNED NOT NULL,
    state SMALLINT(5) UNSIGNED NOT NULL,
    PRIMARY KEY (order_id),
    KEY idx_commerce_order_room_id (room_id),
    KEY idx_commerce_order_account_id (account_id),
    CONSTRAINT fk_commerce_order_room_id
        FOREIGN KEY (room_id)
        REFERENCES commerce_room (room_id) ON DELETE SET NULL ON UPDATE CASCADE,
    CONSTRAINT fk_commerce_order_account_id
        FOREIGN KEY (account_id)
        REFERENCES customer_account (account_id) ON DELETE SET NULL ON UPDATE CASCADE
)
ENGINE=INNODB DEFAULT CHARSET=utf8;

INSERT INTO customer_account (email, password) VALUES ('joe@doe.com', '');

SET @account_id = LAST_INSERT_ID();

INSERT INTO commerce_proprietor (name) VALUES ('ACME');

SET @proprietor_id = LAST_INSERT_ID();

INSERT INTO commerce_property (proprietor_id, name) VALUES (@proprietor_id, 'ACME property');

SET @property_id = LAST_INSERT_ID();

INSERT INTO commerce_room (property_id, name, quantity, price) VALUES (@property_id, 'ACME room', 3, '265.00');

SET @room_id = LAST_INSERT_ID();

INSERT INTO commerce_order (room_id, account_id, from_date, to_date, price, state) VALUES (@room_id, @account_id, '2015-02-07', '2015-02-19', '3445.00', 1);

INSERT INTO commerce_property (proprietor_id, name) VALUES (@proprietor_id, 'Sheraton Hotel');

SET @property_id = LAST_INSERT_ID();

INSERT INTO commerce_room (property_id, name, quantity, price) VALUES (@property_id, 'Classic Double or Twin Room', 2, '425.00');
INSERT INTO commerce_room (property_id, name, quantity, price) VALUES (@property_id, 'Deluxe Double or Twin Room', 6, '360.00');
INSERT INTO commerce_room (property_id, name, quantity, price) VALUES (@property_id, ' Junior Suite ', 1, '190.00');

INSERT INTO commerce_property (proprietor_id, name) VALUES (@proprietor_id, 'Harlem YMCA');

SET @property_id = LAST_INSERT_ID();

INSERT INTO commerce_room (property_id, name, quantity, price) VALUES (@property_id, 'Standard Single Room', 12, '290.00');
INSERT INTO commerce_room (property_id, name, quantity, price) VALUES (@property_id, 'Deluxe Room with Bunk Bed', 4, '310.00');
INSERT INTO commerce_room (property_id, name, quantity, price) VALUES (@property_id, 'Standard Room with Bunk Bed', 3, '270.00');
