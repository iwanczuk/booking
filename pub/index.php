<?php

try {
    require_once __DIR__ . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'src' . DIRECTORY_SEPARATOR . 'bootstrap.php';
} catch (\Exception $e) {
    echo <<<HTML
<!doctype html>
<html>
<head><title>Bootstrap error.</title></head>
<body><h1>Bootstrap error.</h1></body>
</html>
HTML;
    exit(1);
}

\Booking\Framework\Http\Context::init()->handle();

exit;
